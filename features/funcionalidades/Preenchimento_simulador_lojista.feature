#language:pt
@preenchimento_simulador_lojista
Funcionalidade: Validar tela de simulador de propostas do perfil lojista

Contexto:
#Dado que eu esteja no ambiente "http://omnifacil2.omni.com.br/dsv/lucas/acesso"
Dado que eu esteja no ambiente "hml_omni"
E que eu esteja logado no ambiente como "lojista"
E esteja na tela simulador lojista
Quando eu preencho o cpf do cliente lojista
E seleciono o produto "VEÍCULO LEVE - CDC"
E eu preencho os campos da tabela de Veiculo 1
E eu preencho os campos da Proposta de negocio lojista
E eu seleciono a quantidade de parcelas
######################## Preenchimento da proposta ######################################
@preencher_02
Cenario: Preencher proposta com cpf já utilizado em outra proposta preenche automaticamente os campos
Quando eu clico em preencher proposta
E eu preencher o campo cpf com "96827908089"
E eu clico em validar
Entao eu espero visualizar todas as proposta para este cpf
E eu espero que os dados do cliente ja estejam preenchidos

Cenario: Preencher proposta com cpf nunca utilizado em outra proposta
Quando eu clico em preencher proposta
E eu preencho o cpf do cliente
E eu clico em proximo
Entao eu espero visualizar a mensagem "Nada consta em nossa base histórica "
E eu espero que os dados do cliente não venham preenchidos

Cenario: Os dados cadastrados no simulador estão corretos e foram preenchidos automaticamente na tela de cadastro
Quando eu clico em preencher proposta
E eu preencho o cpf do cliente
E eu clico em proximo
Entao eu espero que os campos do simulador estejam preenchidos corretamente

################################# Campos obrigatorios ###################################


# Cenario: Alerta de preenchimento de CRÉDITO Mais é apresentado ao tentar Processar travas
# Quando eu clico em preencher proposta
# E eu preencho o cpf do cliente
# E eu clico em proximo
# E eu preencho os dados obrigatorios da proposta de credito
# E eu clico no botao Processar Travas
# Então eu espero visualizar a mensagem de preenchimento utilização agropecuária

