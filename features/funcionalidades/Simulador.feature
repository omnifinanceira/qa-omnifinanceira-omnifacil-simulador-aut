#language:pt
@simulador
Funcionalidade: Simulador de Ficha Cadastral

Contexto:
Dado que eu esteja no ambiente "hml_omni"

@sss
Cenario: visualizar tabelas de financiamento - usuario loja
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
Entao eu espero visualizar a opção "CDC" na tabela Tipo Financiamento
E eu espero visualizar a opção "CRÉDITO PESSOAL" na tabela Tipo Financiamento
E eu espero visualizar a opção "REFINANCIAMENTO" na tabela Tipo Financiamento

Cenario: visualizar tabelas de financiamento - usuario agente
E eu entre com o usuario "FRANCISCO" e senha "12345qwert"
E esteja na tela simulador loja
Entao eu espero visualizar a opção "CDC" na tabela Tipo Financiamento
E eu espero visualizar a opção "CRÉDITO PESSOAL" na tabela Tipo Financiamento
E eu espero visualizar a opção "REFINANCIAMENTO" na tabela Tipo Financiamento

Cenario: Visualizar as tabelas corretamente
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
Entao eu espero visualizar a tabela "Tipo Financiamento" com opcoes
E eu espero visualizar a tabela "Tabela" com opcoes
E eu espero visualizar a tabela "Produto" com opcoes
E eu espero visualizar a tabela "Tipo Crédito" com opcoes
E eu espero visualizar a tabela "Objeto Financiamento" com opcoes
E eu espero visualizar a tabela "Forma Pagto" com opcoes

Cenario: Visualizar os campos de preenchimento de veiculos
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
Entao eu espero visualizar a tabela "Tipo Financiamento" com opcoes
E eu espero visualizar o campo "Categoria" com opcoes
E eu espero visualizar o campo "Marca" com opcoes
E eu espero visualizar o campo "Ano Modelo" com opcoes
E eu espero visualizar o campo "Modelo" com opcoes
E eu espero visualizar o campo "Versão" com opcoes

Cenario: Os campos de veiculos são preenchidos automaticamente após a seleção dos dados nas tabelas
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
Quando eu preencher os dados na tabela
E eu preencher os dados do veiculo
Então eu espero que o campo "Ano Fab." seja preenchido automaticamente   
E eu espero que o campo "Cotação" seja preenchido automaticamente
E eu espero que o campo "Valor do Bem" seja preenchido automaticamente

Cenario: Ao preencher o campo Valor liberado alguns campos são preenchido 
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
Quando eu preencher os dados na tabela
E eu preencher os dados do veiculo
E eu preencher o campo "Valor Liberado" com "110000"
Então eu espero que o campo "Valor da Entrada" seja preenchido automaticamente   
E eu espero que o campo "% Entrada" seja preenchido automaticamente   
E eu espero que o campo "Valor TC" seja preenchido automaticamente
E eu espero que o campo "Valor a Financiar" seja preenchido automaticamente
E eu espero que o campo "Seguro Contrato" seja preenchido automaticamente

Cenario: Os campos de prestações são apresentados
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
E eu preencher os dados na tabela
E eu preencher os dados do veiculo
Quando eu preencher o campo "Valor Liberado"
E eu selecionar uma opcao no campo "Retorno - Tabela"
Então eu espero visualizar as opcoes de parcelamento

Cenario: Os campos de máximo de financiamento permitido na alçada do agente são preenchidos
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
E eu preencher os dados na tabela
E eu preencher os dados do veiculo
Quando eu preencher o campo "Valor Liberado"
E eu selecionar uma opcao no campo "Retorno - Tabela"
E eu selecionar uma opcao de parcelamento
Entao eu espero visualizar o campo "COTAÇÃO MÁXIMA" ser preenchido automaticamente
E eu espero visualizar o campo "MÁXIMO LIBERADO NA ALÇADA DO AGENTE" ser preenchido automaticamente
E eu espero visualizar o campo "MÁXIMO LIBERADO" ser preenchido automaticamente

Cenario: Ao selecionar a quantidade de prestações alguns campos são preenchido 
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
E eu preencher os dados na tabela
E eu preencher os dados do veiculo
Quando eu preencher o campo "Valor Liberado"
E eu selecionar uma opcao no campo "Retorno - Tabela"
E eu selecionar uma opcao de parcelamento
Entao eu espero visualizar o campo "Fator" ser preenchido automaticamente
E eu espero visualizar o campo "% CET a.a." ser preenchido automaticamente
E eu espero visualizar o campo "Valor IOF" ser preenchido automaticamente
E eu espero visualizar o campo "Valor Contrato" ser preenchido automaticamente
E eu espero visualizar o campo "Valor Total" ser preenchido automaticamente

Cenario: Selecionar mais de um veiculo
E eu entre com o usuario "184BATISTELLA" e senha "senha123"
E esteja na tela simulador loja
E eu preencher os dados na tabela
E eu preencher os dados do veiculo
Quando eu eu clicar no botão de adicionar veiculo
Então eu espero visualizar os campos para adicao de mais um veiculo no simulador















