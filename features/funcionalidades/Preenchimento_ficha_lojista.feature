#language:pt
@preenchimento_ficha_lojista
Funcionalidade: Validar tela de cadastro de proposta

Contexto:
#Dado que eu esteja no ambiente "https://omnifacil2.omni.com.br/dsv/lucas/pck_login.prc_login"
Dado que eu esteja no ambiente "hml_omni"
E que eu esteja logado no ambiente como "lojista"
E esteja na tela simulador lojista
Quando eu preencho o cpf do cliente lojista
E seleciono o produto "VEÍCULO LEVE - CDC"
E eu preencho os campos da tabela de Veiculo 1 - "leve"
E eu preencho os campos da Proposta de negocio lojista
E eu seleciono a quantidade de parcelas
E eu clico em preencher proposta
######################## Preenchimento da proposta ######################################
@obrigatorio_all
Esquema do Cenario: Não preencher campo obrigatorio apresenta alerta ao tentar finalizar a proposta
E eu preencho os campos obrigatórios dos Dados Pessoais - lojista
E eu preencho os campos obrigatórios dos Dados Profissionais - lojista
E eu preencho os campos obrigatórios dos Endereços de correspondencia
E eu preencho os campos obrigatórios das Referencias
Mas deixo o campo <campo> em branco
E eu clicar em "Finalizar e Enviar para a Omni"
Entao eu espero receber um aviso de que o campo é obrigatorio

Exemplos:
|campo|
|"campo_nome"|
|"campo_rg_cliente"|
|"campo_data_rg_cliente"|
|"campo_emissor_rg"|
|"campos_estado_cliente"|
|"campo_sexo"|
|"campo_data_nasc"|
|"campo_estado_civil"|
|"campo_cidade_cliente"|
|"campo_natural_de"|
|"campo_nro_depend"|
|"campo_tipo_moradia"|
|"campo_valor_imovel"|
|"campo_tempo_resid"|
|"campo_ddd_cliente"|
|"campo_fone_cliente"|
|"campo_cat_fone_cliente"|
|"campo_cont_fone_cliente"|
|"campo_ddd_cel_cliente"|
|"campo_cel_cliente"|
|"campo_email_cli"|
|"campo_mae_cli"|

Cenario: Processar travas com sucesso
E eu preencho os campos obrigatórios dos Dados Pessoais - lojista
E eu preencho os campos obrigatórios dos Dados Profissionais - lojista
E eu preencho os campos obrigatórios dos Endereços de correspondencia
E eu preencho os campos obrigatórios das Referencias
E eu clicar em "Finalizar e Enviar para a Omni"
Entao eu espero visualizar a mensagem de aceite do envio da proposta ao agente

################################# Gravar #########################################

Cenario: Gravar dados e deixar a proposta em preenchimento
E eu preencho os campos obrigatórios dos Dados Pessoais - lojista
E eu preencho os campos obrigatórios dos Dados Profissionais - lojista
E eu preencho os campos obrigatórios dos Endereços de correspondencia
E eu preencho os campos obrigatórios das Referencias
E eu clico em gravar
Entao eu espero que a proposta fique com o status "em preenchimento"