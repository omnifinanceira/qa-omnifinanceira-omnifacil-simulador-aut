#language:pt

@decisoes_mesa
Funcionalidade: Decisões na MESA - Perfil MESA

Contexto:
#Dado que eu esteja no ambiente "https://omnifacil2.omni.com.br/dsv/lucas/pck_login.prc_login"
Dado que eu esteja no ambiente "hml_omni"
E que eu esteja logado no ambiente como "mesa"

@pt3 @aprovar_mesa
Cenario: aprovar proposta na mesa! - Agente sem alçada
E eu possuo uma proposta com status "DEVOLVIDA"
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa desejada
E eu seleciono a proposta na Fila da mesa
E eu aceito a decisao pela mesa
E eu realizo o checklist pela mesa
E eu preencho o parecer final pela mesa
Então eu visualizo mensagem de que a proposta foi a fila de aprovacao do supervisor

@pt3 @rejeitar_mesa
Cenario: rejeitar proposta na mesa
E eu possuo uma proposta com status "DEVOLVIDA"
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa desejada
E eu seleciono a proposta na Fila da mesa
E eu rejeito a decisao pela mesa
E eu seleciono o motivo da recusa
E preencho o parecer final
Então eu visualizo mensagem de que a proposta foi recusada

@pt3 @devolver
Cenario: devolver proposta na mesa para agente
E eu possuo uma proposta com status "DEVOLVIDA"
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa desejada
E eu seleciono a proposta na Fila da mesa
E eu devolvo a decisao da proposta ao agente
E eu preencher o MOTIVOS DE DEVOLUÇÃO
E eu clicar em "Gravar"
Entao eu espero visualizar a mensagem Proposta:xxxxxxxxx - "DEVOLVIDA"
##############################################################################

@pt3 @aprovar_mesa_em_analise
Cenario: aprovar proposta na mesa! - Analista sem alçada
E eu possuo uma proposta com status "EM ANALISE"
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa "Fila do Supervisor" em analise
E eu seleciono a proposta na Fila da mesa
E eu aceito a decisao pela mesa
E eu realizo o checklist pela mesa
E eu preencho o parecer final pela mesa
Então eu visualizo mensagem de que a proposta foi a fila de aprovacao do supervisor

@pt3 @rejeitar_mesa_em_analise
Cenario: rejeitar proposta na mesa
E eu possuo uma proposta com status "EM ANALISE"
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa "Fila do Supervisor" em analise
E eu seleciono a proposta na Fila da mesa
E eu rejeito a decisao pela mesa
E eu seleciono o motivo da recusa
E preencho o parecer final
Então eu visualizo mensagem de que a proposta foi recusada

@pt3 @devolver_em_analise
Cenario: devolver proposta na mesa para agente
E eu possuo uma proposta com status "EM ANALISE"
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa "Fila do Supervisor" em analise
E eu seleciono a proposta na Fila da mesa
E eu devolvo a decisao da proposta ao agente
E eu preencher o MOTIVOS DE DEVOLUÇÃO
E eu clicar em "Gravar"
Entao eu espero visualizar a mensagem Proposta:xxxxxxxxx - "DEVOLVIDA"




