#language:pt
@aprovacoes
Funcionalidade: Aprovações das Fases de uma Proposta - Perfil OPERADOR

Contexto:
#Dado que eu esteja no ambiente "http://omnifacil2.omni.com.br/dsv/lucas/acesso"
# Efetua busca no arquivo MASSA
Dado que eu esteja no ambiente "hml_omni"    
E que eu esteja logado no ambiente como "operador"
E eu possuo uma proposta com status "Analise Agente"
E eu esteja na tela da Fila do codigo "184"
E eu selecione a proposta

############################################################################################
################################### CONSULTA BASE HISTORICA ##################################
#############################################################################################

@aprovar_historica
Cenario: Aprovar Consulta Base Historica com sucesso - Operador
E eu acesse a tela "Historica"
Quando eu clicar em "botao_aceitar"
Entao eu espero visualizar a aba "Historica" com a cor "Verde"

@recusar_historica
Cenario: Recusar Consulta Base Historica com sucesso
E eu acesse a tela "Historica"
Quando eu clicar em "botao_recusar"
Entao eu espero visualizar a aba "Historica" com a cor "Vermelha"

@pendencia_historica
Cenario: Status Pendente Consulta Base Historica
E eu acesse a tela "Historica"
Quando eu clicar em "botao_p_interna"
Entao eu espero visualizar a aba "Historica" com a cor "Amarelo"

########################### FICHA / VERIFICAÇÕES #################################
#CENARIO ALTERNATIVO INUTIL....
@verificação_1
Cenario: Aprovar ficha sem confirmar os telefones apresenta mensagens de campos obrigatorios
E eu acesse a tela "Ficha_Verif"
Quando eu clicar em "botao_aceitar" sem confirmacao
Entao eu espero visualizar aviso de preenchimentos obrigatorios

@verificação_2
Cenario: Aprovar Ficha / Verificações com sucesso 
E eu acesse a tela "Ficha_Verif"
Quando eu valido os telefones de contato 
E eu reprocesso as travas
E eu clicar em "botao_aceitar" na tela "Ficha_Verif"
Entao eu espero visualizar a aba "Ficha_Verif" com a cor "Verde" reprocessado

@verificação_3
Cenario: Status Pendente Ficha / Verificações
E eu acesse a tela "Ficha_Verif"
Quando eu valido os telefones de contato 
E eu reprocesso as travas
E eu clicar em "botao_p_interna" na tela "Ficha_Verif"
Entao eu espero visualizar a aba "Ficha_Verif" com a cor "Amarelo" reprocessado

############################ DECISAO ###################################

@decisao_1
Cenario: Recusar decisao com sucesso
E eu acesse a tela "tela_decisao"
Quando eu clicar em "botao_recusar"
E eu selecionar o motivo da recusa 
E eu preencher o parecer final
Entao eu espero visualizar a mensagem Proposta:xxxxxxxxx - "RECUSADA"

@decisao_2
Cenario: Devolver decisao para a origem com sucesso
E eu acesse a tela "tela_decisao"
Quando eu clicar em "botao_devolver_origem"
E eu preencher o MOTIVOS DE DEVOLUÇÃO ORIGEM
E eu clicar em "Gravar"
Entao eu espero visualizar a mensagem Proposta:xxxxxxxxx - "DEVLOJISTA"

@decisao_3
Cenario: Enviar decisao para mesa com sucesso
E eu acesse a tela "tela_decisao"
Quando eu clicar em "botao_enviar_mesa_omni"
E eu selecionar o motivo da devolucao
E eu clicar em "Gravar"
Mas eu não valido o checkout
Entao eu desejo visualizar a mensagem Proposta:xxxxxxxxx - "DEVOLVIDA"

@decisao_4
Cenario: Aceitar decisao com sucesso
Quando eu aceito a Consulta Base Historica
E eu aceito a ficha cadastral
E eu acesse a tela "tela_decisao"
E eu clicar em "botao_aceitar"
E eu realizo o checklist
E eu preencho o parecer final
E eu preencho os dados do usuario com alcada
E eu valido a proposta
Entao eu espero visualizar a mensagem Proposta:xxxxxxxxx - "APROVADA"

