#language:pt
@t
Funcionalidade: Simulador de Ficha Cadastral - CDC

Contexto:
Dado que eu esteja no ambiente "hml_omni"
E que eu esteja logado no ambiente

@gera_proposta_generica
Cenario: Ficha cadastral para CDC gerada com sucesso - Veiculo leve
E esteja na tela simulador
Quando eu seleciono o tipo de financiamento "CDC"
E eu seleciono as informacoes da proposta
E eu preencho os campos da tabela de Veiculo 1
E eu preencho os campos da Proposta de negocio
E eu seleciono a quantidade de parcelas
E eu clico em preencher proposta
E eu preencho o cpf do cliente
E eu clico em proximo
E eu preencho os campos obrigatórios dos Dados Pessoais
E eu preencho os campos obrigatórios dos Dados Profissionais
E eu preencho os campos obrigatórios dos Endereços de correspondencia
E eu preencho os campos obrigatórios das Referencias
E eu preencho os campos obrigatórios da Proposta de Negocio
E eu clico em gravar
E eu processo as travas
Então eu visualizo alerta de que a proposta foi realizada com sucesso

@gera_proposta_generica1
Cenario: Aprovar proposta com sucesso
E eu esteja na tela da Fila
Quando eu seleciono a proposta 
E eu valido os telefones de contato
E eu seleciono opção de credito mais
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu aceito a decisao
E eu realizo o checklist
E eu preencho o parecer final
E eu preencho os dados do usuario com alcada
E eu valido a proposta
Então eu visualizo mensagem de que a proposta foi aprovada

@reprovacao_generica
Cenario: Reprovar proposta com sucesso
E eu esteja na tela da Fila
Quando eu seleciono a proposta 
E eu valido os telefones de contato
E eu seleciono opção de credito mais
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu rejeito a decisao
E eu seleciono o motivo da recusa
E preencho o parecer final
Então eu visualizo mensagem de que a proposta foi recusada
#######################################################################################################
@mesa_sem_checklist_doc @pt1
Cenario: Ficha cadastral para CDC gerada com sucesso - Veiculo leve
E esteja na tela simulador
Quando eu seleciono o tipo de financiamento "CDC"
E eu seleciono as informacoes da proposta
E eu preencho os campos da tabela de Veiculo 1
E eu preencho os campos da Proposta de negocio
E eu seleciono a quantidade de parcelas
E eu clico em preencher proposta
E eu preencho o cpf do cliente
E eu clico em proximo
E eu preencho os campos obrigatórios dos Dados Pessoais
E eu preencho os campos obrigatórios dos Dados Profissionais
E eu preencho os campos obrigatórios dos Endereços de correspondencia
E eu preencho os campos obrigatórios das Referencias
E eu preencho os campos obrigatórios da Proposta de Negocio
E eu clico em gravar
E eu processo as travas
Então eu visualizo alerta de que a proposta foi realizada com sucesso

@mesa_sem_checklist_doc @pt2
Cenario: Enviar proposta a mesa com sucesso
E eu esteja na tela da Fila
Quando eu seleciono a proposta 
E eu valido os telefones de contato
E eu seleciono opção de credito mais
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu acesso a tela de decisao
E eu envio a proposta para Mesa Omni
E eu seleciono o motivo 
E eu seleciono gravar
Então eu visualizo a mensagem que a proposta foi devolvida com sucesso

@pt3 @aprova_mesa
Cenario: aprovar proposta na mesa! - Analista sem alçada
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa desejada
E eu seleciono a proposta na Fila da mesa
E eu aceito a decisao pela mesa
E eu realizo o checklist pela mesa
E eu preencho o parecer final pela mesa
Então eu visualizo mensagem de que a proposta foi a fila de aprovacao do supervisor

@pt3 @rejeitar
Cenario: rejeitar proposta na mesa
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa desejada
E eu seleciono a proposta na Fila da mesa
E eu rejeito a decisao pela mesa
E eu seleciono o motivo da recusa
E preencho o parecer final
Então eu visualizo mensagem de que a proposta foi recusada

@pt3 @devolver
Cenario: devolver proposta na mesa para agente
E eu esteja na tela de Mesa de crédito
Quando eu seleciono a Mesa desejada
E eu seleciono a proposta na Fila da mesa
E eu acesso a tela de decisao
E eu devolvo a proposta ao agente
E eu seleciono o motivo da devolucao
Então eu visualizo mensagem de que a proposta foi devolvida
