#language:pt

@caminho_feliz_agente @hp
Funcionalidade: Simulador de Ficha Cadastral - CDC - Agente

Contexto:
# Busca "hml_omni" e "agente" no arquivo MASSA
Dado que eu esteja no ambiente "hml_omni"
E que eu esteja logado no ambiente como "agente"

# Este cenário foi testado e está gerando a proposta
@gera_proposta_generica_agente
Cenario: Ficha cadastral para CDC gerada com sucesso - Veiculo leve
# E eu selecione a empresa "agente184"
E eu selecione a empresa "agente1247"
E eu esteja na tela do simulador
Quando eu seleciono o tipo de financiamento "CDC"
E eu seleciono as informacoes da proposta
E eu preencho os campos da tabela de Veiculo 1 - "leve-f1"
E eu preencho os campos da Proposta de negocio
E eu seleciono a quantidade de parcelas
E eu clico em preencher proposta
E eu preencho o cpf do cliente
E eu clico em proximo
E eu preencho os campos obrigatórios dos Dados Pessoais
E eu preencho os campos obrigatórios dos Dados Profissionais
E eu preencho os campos obrigatórios dos Endereços de correspondencia
E eu preencho os campos obrigatórios das Referencias
E eu preencho os campos obrigatórios da Proposta de Negocio
E eu preencho o campo de atividade agropecuaria com nao
E eu processo as travas
Então eu visualizo alerta de que a proposta foi realizada com sucesso

@aprova_proposta_generica @validar_massa
Cenario: Aprovar proposta com sucesso
E eu possuo uma proposta com status "Analise Agente"
E eu esteja na tela da Fila do codigo "1247"
Quando eu seleciono a proposta 
E eu valido os telefones de contato
E eu seleciono opção de credito mais
E eu reprocesso as travas
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu aceito a decisao
E eu realizo o checklist
E eu preencho o parecer final
E eu preencho os dados do usuario com alcada
E eu valido a proposta
Então eu visualizo mensagem de que a proposta foi aprovada

@reprovacao_generica
Cenario: Reprovar proposta com sucesso
E eu possuo uma proposta com status "Analise Agente"
E eu esteja na tela da Fila do codigo "184"
Quando eu seleciono a proposta 
E eu valido os telefones de contato
E eu seleciono opção de credito mais
E eu reprocesso as travas
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu rejeito a decisao
E eu seleciono o motivo da recusa
E preencho o parecer final
Então eu visualizo mensagem de que a proposta foi recusada
#######################################################################################################
@envia_proposta_mesa_agente @pt2
Cenario: Enviar proposta a mesa com sucesso
E eu possuo uma proposta com status "Analise Agente"
E eu esteja na tela da Fila do codigo "184"
Quando eu seleciono a proposta
#E eu valido os telefones de contato
E eu seleciono opção de credito mais
E eu reprocesso as travas
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu acesso a tela de decisao
E eu envio a proposta para Mesa Omni
E eu seleciono o motivo 
E eu seleciono gravar
Então eu visualizo a mensagem que a proposta foi devolvida com sucesso


#################################################################################
#                         LOJISTA                                               #
#################################################################################
@aprova_proposta_lojista @validar_massa
Cenario: Aprovar proposta com sucesso - Enviada pelo lojista
E eu possuo uma proposta com status "EM ESPERA"
E eu esteja na tela da Fila do codigo "184"
Quando eu seleciono a proposta
E eu seleciono a operação "6780 - CDC-VL- AAA SC BAT-NR-R4"
#E eu valido os telefones de contato - loj 
E eu preencho o campo de atividade agropecuaria com nao
E eu processo as travas
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu aceito a decisao
E eu realizo o checklist
E eu preencho o parecer final
E eu preencho os dados do usuario com alcada - lojista
E eu valido a proposta
Então eu visualizo mensagem de que a proposta foi aprovada

@reprovacao_generica_lojista
Cenario: Reprovar proposta com sucesso
E eu possuo uma proposta com status "EM ESPERA"
E eu esteja na tela da Fila do codigo "184"
Quando eu seleciono a proposta
E eu seleciono a operação "6780 - CDC-VL- AAA SC BAT-NR-R4"
E eu valido os telefones de contato - loj
E eu preencho o campo de atividade agropecuaria com nao
E eu processo as travas
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu rejeito a decisao
E eu seleciono o motivo da recusa
E preencho o parecer final
Então eu visualizo mensagem de que a proposta foi recusada

@reprovacao_generica_lojista
Cenario: Reprovar proposta com sucesso
E eu possuo uma proposta com status "EM ESPERA"
E eu esteja na tela da Fila do codigo "184"
Quando eu seleciono a proposta
E eu seleciono a operação "6780 - CDC-VL- AAA SC BAT-NR-R4"
E eu valido os telefones de contato - loj
E eu preencho o campo de atividade agropecuaria com nao
E eu processo as travas
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu rejeito a decisao
E eu seleciono o motivo da recusa
E preencho o parecer final
Então eu visualizo mensagem de que a proposta foi recusada

@envia_proposta_do_lojista_para_mesa @pt2
Cenario: Enviar proposta do lojista para a analise na mesa
E eu possuo uma proposta com status "EM ESPERA"
E eu esteja na tela da Fila do codigo "184"
Quando eu seleciono a proposta
#E eu valido os telefones de contato
E eu seleciono opção de credito mais
E eu processo as travas
E eu aceito a Consulta Base Histórica
E eu aceito a ficha cadastral
E eu acesso a tela de decisao
E eu envio a proposta para Mesa Omni
E eu seleciono o motivo 
E eu seleciono gravar
Então eu visualizo a mensagem que a proposta foi devolvida com sucesso

