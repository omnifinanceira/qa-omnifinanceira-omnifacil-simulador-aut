Dado("que eu esteja no ambiente {string}") do |ambiente|
  visit MASSA[ambiente]  # 
  sleep 3
end

Dado("que eu esteja logado no ambiente como {string}") do |string|
  @frame = string # gambiarra pra funcionar os Frames que são despadronizados (bottomFrame e frm-principal)
  find("#p-nome", wait: 5).set(MASSA[string]["login"])
  find("#p-senha", wait: 5).set(MASSA[string]["senha"])
  click_button "Conectar"
end

# Acesso do Agente
# Não existe Agente com somente um código por isso invalidei este código que tinha
# Criado para prever este tipo de situação - 05/10/2020
Dado('eu selecione a empresa') do |agente|
   find(ELEMENTOS["novo_menu"]).click
   find("select[name='P_EMP']").find("option[value= #{MASSA[agente]} ]").select_option
   click_button("Validar")
end

Dado("eu esteja na tela do simulador") do
  # Este find foi comentado pois já está sendo chamado no passo acima
  #find(ELEMENTOS["novo_menu"]).click
  #campo_emp = all("select[name='P_EMP']")
  #unless campo_emp.empty?
  # find("select[name='P_EMP']").find("option[value='1247']").select_option
  # click_button("Validar")
  #end
  operacional = find("#navbar-collapse-1").find(".dropdown", text: "OPERACIONAL").click
  operacional.assert_selector(".dropdown.dropdown-submenu")
  credito = operacional.find(".dropdown.dropdown-submenu", text: "CRÉDITO").click
  credito.find("li", text: "Simulador").click
end

# Acesso do Operador
Dado("eu esteja na tela do simulador operador") do
  find(ELEMENTOS["novo_menu"]).click
  operacional = find("#navbar-collapse-1").find(".dropdown", text: "OPERACIONAL").click
  operacional.assert_selector(".dropdown.dropdown-submenu")
  credito = operacional.find(".dropdown.dropdown-submenu", text: "CRÉDITO").click
  credito.find("li", text: "Simulador").click
end

Dado("esteja na tela simulador loja") do
  find(ELEMENTOS["novo_menu"]).click
  operacional = find("#navbar-collapse-1").find(".dropdown", text: "OPERACIONAL").click
  operacional.assert_selector(".dropdown.dropdown-submenu")
  credito = operacional.find(".dropdown.dropdown-submenu", text: "CRÉDITO").click
  credito.find("li", text: "Simulador").click
end

Quando("eu seleciono o tipo de financiamento {string}") do |opcao|
  within_frame(ELEMENTOS[@frame]) do
    find("#p_grupo1").find("option[value='#{opcao}']").select_option
  end
end

Quando("eu seleciono as informacoes da proposta") do
  within_frame(ELEMENTOS[@frame]) do
    @cdc_met.preenche_tabelas
  end
end

Quando("eu preencho os campos da tabela de Veiculo {int}") do |int|
  within_frame(ELEMENTOS[@frame]) do
    @cdc_met.preenche_dados_veiculo
  end
end

Quando("eu preencho os campos da Proposta de negocio") do
  sleep 3
  within_frame(ELEMENTOS[@frame]) do
    valor = find("input[name='P_VALOR_COTACAO1']").value.delete("^0-9,").to_f
    fill_in("P_PROP_VLR_LIBERADO", with: ((valor * 0.7).to_i + 1) * 100)
    @valor_lib = find("input[name='P_PROP_VLR_LIBERADO']").value
    find("input[name='P_PROP_VLR_ENT']").click
    find("select[name='P_RETORNO']").all("option")[1].select_option
  end
end

Quando("eu preencho os campos da Proposta de negocio lojista") do
  within_frame(ELEMENTOS[@frame]) do
    valor = find("input[name='P_PROP_VLR_COMP']").value.delete("^0-9,").to_f
    fill_in("P_PROP_VLR_ENT", with: ((valor * 0.7).to_i + 1) * 100)
    find("input[name='P_PROP_VLR_PREST']").click
  end
end

Quando("eu seleciono a quantidade de parcelas") do
  within_frame(ELEMENTOS[@frame]) do
    find("#div_prest").find("input[value='36']").click
  end
end

Quando("eu clico em preencher proposta") do
  within_frame(ELEMENTOS[@frame]) do
    click_button("Preencher Proposta")
  end
end

Quando("eu preencho o cpf do cliente") do
  within_frame(ELEMENTOS[@frame]) do ## no agente é bottomFrame
    #@cpf = FFaker::IdentificationBR.cpf
    #find("input[name='P_CPF_CLI']").set(@cpf)
    find("input[name='P_CPF_CLI']").set("58683268063")
    click_button "Validar"
  end
end

Quando("eu clico em proximo") do
  within_frame(ELEMENTOS[@frame]) do
    click_button "Próximo"
  end
end

Quando("eu preencho os campos obrigatórios dos Dados Pessoais") do
  within_frame(ELEMENTOS[@frame]) do
    # binding.pry
    # @proposta = find(:xpath, ELEMENTOS["campo_nro_proposta"], wait: 5).text
    @cdc_met.preenche_dados_operador
    @cdc_met.preenche_dados_pessoais
    @commons.pega_cep_cli
    @commons.foca_tela
  end
end

Quando("eu preencho os campos obrigatórios dos Dados Profissionais") do
  within_frame(ELEMENTOS[@frame]) do
    @cdc_met.preenche_dados_prof
    sleep 4
    @commons.pega_cep_prof
    sleep 2
    @commons.foca_tela
  end
end

Quando("eu preencho os campos obrigatórios dos Endereços de correspondencia") do
  within_frame(ELEMENTOS[@frame]) do
    all("input[name='P_CORRESP_CLI']")[0].click
  end
end

Quando("eu preencho os campos obrigatórios das Referencias") do
  within_frame(ELEMENTOS[@frame]) do
    @cdc_met.preenche_proposta_dados_referencias
    #binding.pry
  end
end

Quando("eu preencho os campos obrigatórios da Proposta de Negocio") do
  within_frame(ELEMENTOS[@frame]) do
    find("select[name='P_TAXA_RETORNO']").all("option")[1].select_option
  end
end

Quando("eu preencho o campo de atividade agropecuaria com nao") do
  within_frame(ELEMENTOS[@frame]) do
    find("#divAgropecuaria").find("input[value='N']").click
    @commons.gravar_string(@proposta, @cpf, @valor_lib)
  end
end

# Quando("eu clico em gravar") do
#   within_frame(ELEMENTOS[@frame]) do
#     click_button "Gravar"
#     @commons.valida_alert
#     assert_no_selector("#waitDialog", wait: 300)
#     assert_no_selector(".sub-frame-error")
#   end
#   sleep 3
# end

Quando("eu processo as travas") do
  within_frame(ELEMENTOS[@frame]) do
    click_button("Processar Travas", wait: 500)
    @commons.valida_alert
    sleep 3
  end
end

Quando("eu processo as travas - lojista") do
  within_frame(ELEMENTOS[@frame]) do
    click_button("Processar Travas", wait: 500)
    @commons.valida_alert
    sleep 3
  end
end

Então("eu visualizo alerta de que a proposta foi realizada com sucesso") do
  within_frame(ELEMENTOS[@frame]) do
    raise "PAGINA NAO RESPONDEU" unless assert_no_selector(".main-frame-error") == true
    #expect(page).to have_content 'foi gerada com sucesso porém existe(m) restrição(ções)'
    assert_text("foi gerada com sucesso porém existe(m) restrição(ões)")
    #assert_no_text("foi gerada com sucesso porém existe(m) restrição(ões).", wait: 10, exact: false)
    # proposta = File.new("data/proposta.txt", "w")
    # proposta.puts "#{@proposta.delete("^0-9")}"
    # proposta.close
  end
end
################################### Aprovações ######################################
Dado("eu esteja na tela da Fila do codigo {string}") do |string|
  find(ELEMENTOS["novo_menu"]).click
  find("select[name='P_EMP']").find("option[value='#{string}']").select_option
  click_button("Validar")
  operacional = find("#navbar-collapse-1").find(".dropdown", text: "OPERACIONAL").click
  operacional.assert_selector(".dropdown.dropdown-submenu")
  credito = operacional.find(".dropdown.dropdown-submenu", text: "CRÉDITO").click
  credito.all("li", text: "Fila")[0].click
end

Dado("eu esteja na tela da Fila") do
  #binding.pry
  operacional = find("#navbar-collapse-1").find(".dropdown", text: "OPERACIONAL").click
  operacional.assert_selector(".dropdown.dropdown-submenu")
  credito = operacional.find(".dropdown.dropdown-submenu", text: "CRÉDITO").click
  credito.find("li", text: "Fila").click
end

Quando("eu seleciono a proposta") do
  within_frame(ELEMENTOS[@frame]) do
    within_frame("bottomFrame2") do
      binding.pry
      linha = all("tr", text: @proposta, wait: 30)[1]
      within(linha) { find("input[type='button']").click }
      #fill_in("Procurar", with: @proposta)
      #linha = find("tr", text: @proposta, wait: 30)
      #linha.find("a[title='Analise Agente']").click
      @commons.valida_alert
    end
  end
end

Quando("eu seleciono a proposta NOVO MODELO") do
  within_frame(ELEMENTOS[@frame]) do
    fill_in("Procurar", with: @proposta)
    linha = find("tr", text: @proposta, wait: 30)
    linha.find(".btn.btn-sm.btn-default").click
    @commons.valida_alert2
  end
end

Quando("eu valido os telefones de contato") do
  within_frame(ELEMENTOS[@frame]) do
    find(ELEMENTOS["Ficha_Verif"]).click
    @cdc_met.confirma_dados_tel
  end
  sleep 2
end

Quando("eu valido os telefones de contato - loj") do
  within_frame(ELEMENTOS[@frame]) do
    find(ELEMENTOS["Ficha_Verif"]).click
    @cdc_met.confirma_dados_tel_2
  end
end

Quando("eu seleciono opção de credito mais") do
  within_frame(ELEMENTOS[@frame]) do
    find(ELEMENTOS["Ficha_Verif"]).click #TIRAR SAPORRA DEPOIS QUE RODAR AS ALÇADAS
    find(:xpath, "//input[@name=\"P_COMPRA_DIVIDA\"][@value=\"N\"]").click
    #   assert_selector("#btReprocessarTravas", wait: 7)
    #   find("#btReprocessarTravas").click
    #   assert_no_selector("#popup_container", text: "Esta proposta encontra-se restrita.")
    #   assert_no_selector(".main-frame-error", wait: 300)
    #   assert_selector("#popup_container")
    #   find("#popup_ok").click
  end
  # @commons.foca_tela()
  # @commons.valida_alert
end

Quando("eu aceito a Consulta Base Histórica") do
  # no cenario @decisao_4 eh necessario colocar dentro do iframe
  @commons.foca_tela()
  find(ELEMENTOS["Historica"]).click
  sleep 3
  find(ELEMENTOS["botao_aceitar"]).click
  @commons.valida_alert
  find(ELEMENTOS["Ficha_Verif"], wait: 10).click
  expect(find(ELEMENTOS["Historica"])["class"]).to eq("botaoVerde")
end

Quando("eu aceito a ficha cadastral") do
  find(ELEMENTOS["Ficha_Verif"]).click
  find(ELEMENTOS["botao_aceitar"]).click
  @commons.valida_alert
  assert_no_selector("#waitDialog", wait: 300)
end

Quando("eu aceito a decisao") do
  sleep 2
  find(ELEMENTOS["tela_decisao"]).click
  find(ELEMENTOS["botao_aceitar"]).click
  @commons.valida_alert
end

Quando("eu realizo o checklist") do
  find(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']").click
  find(:xpath, "//input[@value='#012_CRV - CERTIFICADO DE REGISTRO DO VEÍCULO']").click
end

Quando("eu preencho o parecer final") do
  find(ELEMENTOS["parecer_final"]).set("testes Automatizados")
  sleep 3
  find(:xpath, ELEMENTOS["botao_validar"]).click
  @commons.valida_alert
end

Então("eu preencho os dados do usuario com alcada") do
  find(:xpath, ELEMENTOS["usuario_alcada"]).all("option").sample.select_option
  find(:xpath, ELEMENTOS["senha_alcada"]).set(MASSA["senha_alcada"])
end

Então("eu preencho os dados do usuario com alcada - lojista") do
  find(:xpath, ELEMENTOS["usuario_alcada"]).all("option").sample.select_option
  find(:xpath, ELEMENTOS["senha_alcada"]).set("senha123")
end

Quando("eu valido a proposta") do
  find(:xpath, ELEMENTOS["botao_validar"]).click
end

Então("eu visualizo mensagem de que a proposta foi aprovada") do
  assert_text("Proposta:#{@proposta} - APROVADA")
  proposta = File.new("data/proposta.txt", "w")
  proposta.close
  @commons.gravar_string(@proposta, @cpf, "APROVADA")
end

Quando("eu rejeito a decisao") do
  @commons.foca_tela()
  find(ELEMENTOS["tela_decisao"]).click
  find(ELEMENTOS["botao_recusar"]).click
  @commons.valida_alert
end

############# REFINANCIAMENTO ########################################

Quando("eu seleciono as informacoes da proposta - Refinanciamento") do
  within_frame(ELEMENTOS[@frame]) do
    find(ELEMENTOS["Tabela"]).find("option[value='69']").select_option
    find(ELEMENTOS["Produto"]).find("option[value='VEÍCULO LEVE']").select_option
    find(ELEMENTOS["Tipo_credito"]).find("option[value='NORMAL']").select_option
    find(ELEMENTOS["Objeto_financiamento"]).find("option[value='REFI-VL-A-NR-R4']").select_option
  end
end

Quando("eu preencho os campos obrigatórios da Proposta de Negocio - Refinanciamento") do
  within_frame(ELEMENTOS[@frame]) do
    find(:xpath, "//input[@name='P_PROP_NUM_PREST']").set("36")
    find(:xpath, "//input[@name='P_COMISSAO']").click
    find(:xpath, "//select[@name='P_TAXA_RETORNO']").all("option")[1].select_option
  end
end

##################### ENVIAR A MESA ############################################

Quando("eu envio a proposta para Mesa Omni") do
  find(".botaoLaranja").click
  @commons.valida_alert
end

Quando("eu seleciono o motivo") do
  linhas = all(:xpath, "//tr[@class='fundoAzul']").sample
  selecionado = linhas.all("td").sample
  @motivo = selecionado.find("input").value
  selecionado.find("input").click
  find("textarea[name='P_OBSERVACAO_AGENTE']").set("aprova ai")
end

Quando("eu seleciono gravar") do
  click_button "Gravar"
  @commons.valida_alert
end

Quando("eu acesso a tela de decisao") do
  @commons.foca_tela
  #within_frame(ELEMENTOS[@frame]) do QUEBRA TESTE @envia_proposta_mesa
  find(ELEMENTOS["tela_decisao"]).click
  #end
end

Quando("eu não refaço o checklist") do
  @commons.valida_alert2
end

Então("eu visualizo a mensagem que a proposta foi devolvida com sucesso") do
  @commons.foca_tela()
  find("#prn").click
  @commons.valida_alert
  sleep 8
  @commons.foca_tela()
  p texto = find("center").text
  expect(texto.include?(@proposta.to_s)).to eq true
  @commons.gravar_string(@proposta, @cpf, "DEVOLVIDA MESA")
end
#################### APROVAR MESA #####################################
Dado("eu esteja na tela de Mesa de crédito") do
  find(ELEMENTOS["mesa_de_credito"]).click
end

Quando("eu seleciono a Mesa desejada") do
  within_frame(ELEMENTOS[@frame]) do
    #binding.pry
    find("#fila27_1").find("#priDadosA27_1").find("#infoEspera27_1").click
  end
end

Quando("eu seleciono a Mesa {string} em analise") do |string|
  within_frame(ELEMENTOS[@frame]) do
    find("#fila1_1").find(ELEMENTOS[string]).find("#shapeAnalise1_1").click
  end
end

Quando("eu seleciono a proposta na Fila da mesa") do
  @commons.foca_tela()
  within("#tblListaProposta") do
    #binding.pry
    find("td", text: @proposta, wait: 20).click
  end
end

Quando("eu aceito a decisao pela mesa") do
  @commons.foca_tela()
  within_frame(ELEMENTOS[@frame]) do
    all(ELEMENTOS["tela_decisao"], wait: 20)[0].click
    find(ELEMENTOS["botao_aceitar"], wait: 20).click
    @commons.valida_alert
  end
end

Quando("eu realizo o checklist pela mesa") do
  @commons.foca_tela()
  within_frame(ELEMENTOS[@frame]) do
    assert_selector(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']")
    find(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']").click
    assert_selector(:xpath, "//input[@value='#012_CRV - CERTIFICADO DE REGISTRO DO VEÍCULO']")
    find(:xpath, "//input[@value='#012_CRV - CERTIFICADO DE REGISTRO DO VEÍCULO']").click
  end
end

Quando("eu preencho o parecer final pela mesa") do
  @commons.foca_tela()
  within_frame(ELEMENTOS[@frame]) do
    find(ELEMENTOS["parecer_final"]).set("testes Automatizados")
    find(:xpath, ELEMENTOS["botao_validar"]).click
    @commons.valida_alert
  end
end

Então("eu visualizo mensagem de que a proposta foi a fila de aprovacao do supervisor") do
  @commons.foca_tela()
  within_frame(ELEMENTOS[@frame]) do
    texto = find("center").text
    expect(texto.include?("Proposta:#{@proposta} - Enviada para fila de Aprovação do Supervisor.")).to eq true
  end
end
#################### RECUSA MESA ##################################################
Quando("eu rejeito a decisao pela mesa") do
  @commons.foca_tela()
  within_frame(ELEMENTOS[@frame]) do
    find(ELEMENTOS["botao_decisao"], wait: 20).click
    sleep 4
    find(ELEMENTOS["botao_recusar"], wait: 20).click
    @commons.valida_alert
  end
end

Quando("eu seleciono o motivo da recusa") do
  @commons.foca_tela()
  within_frame(ELEMENTOS[@frame]) do # para recusa lojista nao há o frm
    @dropdown = find("select[name='p_motivo_recusa1']", wait: 15)
    @opcao = @dropdown.all("option").sample.text
    select(@opcao, :from => "p_motivo_recusa1")
  end
end

Quando("preencho o parecer final") do
  within_frame(ELEMENTOS[@frame]) do
    find(ELEMENTOS["parecer_final"]).set("Recusado automatizado")
    click_button("Validar")
    @commons.valida_alert
  end
end

Então("eu visualizo mensagem de que a proposta foi recusada") do
  @commons.foca_tela()
  within_frame(ELEMENTOS[@frame]) do
    texto = find("center").text
    expect(texto.include?("Proposta:#{@proposta} - RECUSADA")).to eq true
  end
end
#################################### DEVOLVER ########################################
Quando("eu devolvo a decisao da proposta ao agente") do
  @commons.foca_tela()
  within_frame(ELEMENTOS[@frame]) do
    #binding.pry
    find(ELEMENTOS["botao_decisao"]).click
    find(ELEMENTOS["botao_devolver"]).click
    @commons.valida_alert
  end
end

#####################################################################################
########################### CAMINHO FELIZ LOJISTA #####################################
Dado("esteja na tela simulador lojista") do
  find(".menuBar").all("a", text: "Simulador")[0].click
end

Quando("eu preencho o cpf do cliente lojista") do
  within_frame(ELEMENTOS[@frame]) do
    @cpf = FFaker::IdentificationBR.cpf
    @commons.find_ext("input", "name", "P_CPF_CLI").set(@cpf)
  end
end

Quando("seleciono o produto {string}") do |string|
  within_frame(ELEMENTOS[@frame]) do
    find("#cboGrupo").find("option", text: string).select_option
  end
end

Quando("eu preencho os campos obrigatórios dos Dados Pessoais - lojista") do
  within_frame(ELEMENTOS[@frame]) do
    #binding.pry
    @proposta = find(:xpath, ELEMENTOS["campo_nro_proposta_lojista"], wait: 5).text
    @cdc_met.preenche_dados_pessoais
    find("#P_CEP_INI_CLI").set("04003")
    find("#P_CEP_FIM_CLI").set("010")
    find(:xpath, "/html/body/form[3]/table[5]/tbody/tr[4]/td/table/tbody/tr/td[1]/input").click
    @commons.foca_tela
    find("input[name='P_NUMERO']").set("10")
    click_button("Confirmar")
    @commons.foca_tela
  end
end

Quando("eu preencho os campos obrigatórios dos Dados Profissionais - lojista") do
  within_frame(ELEMENTOS[@frame]) do
    find("select[name='P_PROF_GRAU_INSTR']").find("option[value='2']").select_option
    find("select[name='P_PROF_CLASSE']").find("option[value='1']").select_option
    find("#P_BSC_PROF").set("ANALISTA DE SISTEMAS")
    find(:xpath, "/html/body/form[3]/table[7]/tbody/tr[2]/td/table/tbody/tr/td[1]/a/img").click
    sleep 5
    find("#P_PROF_EMP").set("Omni")
    find("input[name='P_PROF_SAL']").set("180000")
    find("#P_PROF_TEMP").set("052010")
    find("#P_PROF_FONE_DDD").set("11")
    find("#P_PROF_FONE").set("55889966")
    sleep 4
    find("#P_PROF_CEP_INI").set("04003")
    find("#P_PROF_CEP_FIM").set("010")
    find(:xpath, "/html/body/form[3]/table[7]/tbody/tr[5]/td/table/tbody/tr/td[1]/input[3]").click
    @commons.foca_tela
    find("input[name='P_NUMERO']").set("10")
    click_button("Confirmar")
    sleep 2
    @commons.foca_tela
  end
end

Quando("confirmo o envio para a omni") do
  within_frame(ELEMENTOS[@frame]) do
    assert_selector("#popup_container", text: "Deseja salvar as informações e enviar a proposta para a Omni?", wait: 15)
    find("#popup_ok").click
    @commons.valida_alert
  end
end

Quando("eu seleciono a operação {string}") do |string|
  within_frame(ELEMENTOS[@frame]) do
    assert_selector("#popup_container")
    find("#popup_ok").click
    find("#popup_ok").click
    operacoes = find("select[name='P_OPERACAO']").all("option")
    achou = true
    i = 0
    while achou != false
      achou = operacoes[i].text.include?("BAL")
      i = i + 1
    end
    operacoes[i].select_option
    @commons.valida_alert
  end
end

Quando("eu preencho os campos da tabela de Veiculo {int} - {string}") do |int, string|
  within_frame(ELEMENTOS[@frame]) do
    @cdc_met.preenche_dados_veiculo(int, string)
  end
end
