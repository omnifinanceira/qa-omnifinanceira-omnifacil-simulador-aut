Dado("eu possuo uma proposta com status {string}") do |string|
  puts @proposta = @commons.faz_select(BANCO[string])
end

Dado("eu selecione a proposta") do
  within_frame("bottomFrame") do
    within_frame("bottomFrame2") do
      @linha = all("tr", text: @proposta, wait: 30)[1]
      @linha.find("input[type='button']").click
    end
  end
end

Dado("eu acesse a tela {string}") do |string|
  within_frame("bottomFrame") do
    assert_selector(ELEMENTOS[string])
    find(ELEMENTOS[string]).click
    sleep 4
  end
end

Quando("eu clicar em {string}") do |string|
  within_frame("bottomFrame") do # quebrando no teste @decisao_4
    #within_frame("bottomFrame") do
    #binding.pry
    find(ELEMENTOS[string]).click
    @commons.valida_alert
  end
end

Quando("eu clicar em {string} sem confirmacao") do |string|
  within_frame("bottomFrame") do
    find(ELEMENTOS[string]).click
  end
end

Quando("eu reprocesso as travas") do
  within_frame("bottomFrame") do
    @cdc_met.reprocessar_travas
  end
  @commons.foca_tela()
  @commons.valida_alert
  sleep 6
end

Entao("eu espero visualizar a aba {string} com a cor {string}") do |nome_aba, classe|
  within_frame("bottomFrame") do
    esperado = find(ELEMENTOS[nome_aba])["class"]
    expect(esperado).to eql(ELEMENTOS[classe])
  end
end

Entao("eu espero visualizar a aba {string} com a cor {string} reprocessado") do |nome_aba, classe|
  esperado = find(ELEMENTOS[nome_aba])["class"]
  expect(esperado).to eql(ELEMENTOS[classe])
end

Quando("eu clicar em {string} na tela {string}") do |botao, nome_aba|
  @commons.foca_tela()
  find(ELEMENTOS[nome_aba]).click
  assert_selector(ELEMENTOS[botao])
  find(ELEMENTOS[botao]).click
  @commons.valida_alert
end

Quando("eu selecionar o motivo da devolucao") do
  within_frame("bottomFrame") do
    linhas = all(:xpath, "//tr[@class='fundoAzul']").sample
    selecionado = linhas.all("td").sample
    @motivo = selecionado.find("input").value
    selecionado.find("input").click
    find("textarea[name='P_OBSERVACAO_AGENTE']").set("aprova ai")
  end
end

Entao("eu espero visualizar aviso de preenchimentos obrigatorios") do
  within_frame("bottomFrame") do
    assert_selector("#popup_content")
    assert_selector("#popup_message", text: "Para aceitar, todas as informações abaixo deverão estar preenchidas:")
  end
end

Quando("eu selecionar o motivo da recusa") do
  within_frame("bottomFrame") do
    @dropdown = find("select[name='p_motivo_recusa1']", wait: 15)
    @opcao = @dropdown.all("option").sample.text
    select(@opcao, :from => "p_motivo_recusa1")
  end
end

Quando("eu preencher o parecer final") do
  within_frame("bottomFrame") do
    find(ELEMENTOS["parecer_final"]).set("Recusado automatizado")
    click_button("Validar")
    @commons.valida_alert
  end
end

Entao("eu espero visualizar a mensagem Proposta:xxxxxxxxx - {string}") do |string|
  # within_frame("bottomFrame") do #quebrando no @decisao_1 usa o bottomFrame
  within_frame("bottomFrame") do
    texto = find("center", wait: 15).text
    expect(texto.include?("Dados enviados com sucesso!")).to eq true
    expect(texto.include?("Proposta:")).to eq true
    expect(texto.include?(string)).to eq true
  end
end

Entao("eu desejo visualizar a mensagem Proposta:xxxxxxxxx - {string}") do |string|
  texto = find("center").text
  expect(texto.include?("Dados enviados com sucesso!")).to eq true
  expect(texto.include?("Proposta:")).to eq true
  expect(texto.include?(string)).to eq true
end

Quando("eu preencher o MOTIVOS DE DEVOLUÇÃO ORIGEM") do
  within_frame("bottomFrame") do
    fill_in("P_INF_ADICIONAIS", with: "Devolveu")
  end
end

Quando("eu preencher o MOTIVOS DE DEVOLUÇÃO") do
  #  within_frame("bottomFrame") do #quebrando no @decisao_2 usa bottomFrame
  within_frame("bottomFrame") do
    #binding.pry
    tabela = all(".tabBorda1")
    motivo = tabela[1].all("tr")
    motivo.sample.all("td").sample.find("input").click
    @commons.valida_alert
  end
end

Quando("eu não valido o checkout") do
  sleep 3
  @commons.foca_tela()
  find("#prn").click
  @commons.valida_alert
  @commons.foca_tela()
end
