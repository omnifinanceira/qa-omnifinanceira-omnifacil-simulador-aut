Dado("eu preencho os campos da Proposta de negocio com valor liberado {string}") do |valor|
  within_frame("frm-principal") do
    find("input[name='P_PROP_VLR_LIBERADO']").set(valor)
    @valor_lib = find("input[name='P_PROP_VLR_LIBERADO']").value
  end
end

Então("eu espero que o campo {string} esteja com o mesmo valor da simulacao") do |string|
  within_frame("frm-principal") do
    expect(find("input[name='P_VLR_FINANCIADO']").value).to eq @valor_lib
  end
end

Quando("eu preencher o campo cpf com {string}") do |cpf|
  within_frame("frm-principal") do
    @commons.find_ext("input", "name", "P_CPF_CLI").set(cpf)
  end
end

Quando("eu clico em validar") do
  within_frame("frm-principal") do
    click_button "Validar"
  end
end

Entao("eu espero visualizar todas as proposta para este cpf") do
  within_frame("frm-principal") do
    find("table[class='tabBorda1']").find("th", text: "Proposta(s) com o mesmo CPF")
    click_button "Próximo"
  end
end

Entao("eu espero que os dados do cliente ja estejam preenchidos") do
  within_frame("frm-principal") do
    expect(find(CAMPOS_PROPOSTA["campo_nome"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_rg_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_data_rg_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_emissor_rg"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campos_estado_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_sexo"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_data_nasc"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_estado_civil"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_cidade_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_nro_depend"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_valor_imovel"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_tempo_resid"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_ddd_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_fone_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_cont_fone_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_ddd_cel_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_cel_cliente"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_email_cli"]).value).not_to eql("")
    expect(find(CAMPOS_PROPOSTA["campo_placa_vei"]).value).not_to eql("")
  end
end

Quando("deixo o campo campo_nome em branco") do
end

Entao("eu espero visualizar a mensagem {string}") do |string|
  pending # Write code here that turns the phrase above into concrete actions
end

Entao("eu espero que os dados do cliente não venham preenchidos") do
  pending # Write code here that turns the phrase above into concrete actions
end

Entao("eu espero que os campos do simulador estejam preenchidos corretamente") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("eu preencho os dados da proposta de credito") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("deixo o campo {string} em branco") do |string|
  @campo_limpo = string
  within_frame(ELEMENTOS[@frame]) do
    campo = find(CAMPOS_PROPOSTA[string])
    if campo.tag_name == "select"
      campo.all("option")[0].select_option
    else
      campo.set("")
    end
    #
  end
end

Entao("eu espero receber um aviso de que o campo é obrigatorio") do
  #binding.pry
  within_frame(ELEMENTOS[@frame]) do
    assert_selector("#popup_container", text: MENSAGENS["mensagens_erro_ficha"][@campo_limpo], wait: 15)
  end
end

Quando("eu preencho os dados obrigatorios da proposta de credito") do
  pending # Write code here that turns the phrase above into concrete actions
end

Então("eu espero visualizar a mensagem de preenchimento utilização agropecuária") do
  binding.pry
end

Quando("eu preencho o campo de Utilização Agropecuária") do
  pending # Write code here that turns the phrase above into concrete actions
end

Então("eu espero visualizar um alerta de que os dados da proposta foram atualizados") do
  pending # Write code here that turns the phrase above into concrete actions
end

Entao("eu espero que a proposta fique com o status {string}") do |string|
  pending # Write code here that turns the phrase above into concrete actions
end
