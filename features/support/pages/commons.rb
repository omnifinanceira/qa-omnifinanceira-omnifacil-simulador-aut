class Commons
  include Capybara::DSL

  def find_ext(tag, atributo, valor)
    coisa = find("#{tag}[#{atributo}='#{valor}']", wait: 7)
    return coisa
  end

  def gravar_string(proposta, cpf, valor)
    File.open("data/log.txt", "a") { |f| f.write("\n#{proposta}, #{cpf}, #{valor}, #{Time.now}") }
  end

  def gravar_proposta(proposta)
  end

  def gravar_string(proposta, cpf, valor)
    File.open("data/log.txt", "a") { |f| f.write("\n#{proposta}, #{cpf}, #{valor}") }
  end

  def pega_cep_cli()
    find("#P_CEP_INI_CLI").set("04003")
    find("#P_CEP_FIM_CLI").set("010")
    # binding.pry
    all(ELEMENTOS["botao_consultar_cep"])[0].click #tem que clicar no botao
    foca_tela
    find("input[name='P_NUMERO']").set("10")
    click_button("Confirmar")
  end

  # def clicar_botao_OK
  #   if page.has_css?("#popup_container") 
  #     click_button("OK")
  #   end
  # end

  def pega_cep_prof
    find("#P_PROF_CEP_INI").set("04003")
    find("#P_PROF_CEP_FIM").set("010")
    all(ELEMENTOS["botao_consultar_cep"])[1].click
    foca_tela
    find("input[name='P_NUMERO']").set("10")
    click_button("Confirmar")
  end

  def foca_tela()
    result = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(result)
  end

  def valida_alert
    begin
      retries ||= 0
      "vam ve"
      puts "#{page.driver.browser.switch_to.alert.text} SIM."
      page.accept_alert
      raise "PAGINA NAO RESPONDEU" unless assert_no_selector(".main-frame-error") == true
    rescue
      p "sera?"
      retry if (retries += 1) < 2
    end
  end

  def valida_alert2
    begin
      "vam ve"
      page.dismiss_confirm
      #puts "#{page.driver.browser.switch_to.alert.text} SIM."
      raise "PAGINA NAO RESPONDEU" unless assert_no_selector(".sub-frame-error") == true
    rescue
      p "sera?"
      retry
    end
  end

  def screenshot(page)
    screenshot = page.save_screenshot("data/screenshots/#{Time.now.strftime("%Y%m%d")}/#{Time.now.strftime("%Y%m%d%H%M%S")}.png")
    embed("#{screenshot}", "image/png")
  end

  def faz_select(query)
    conn = OCI8.new(BANCO["user"], BANCO["pass"], BANCO["ambiente"])
    proposta = []
    cursor = conn.parse(query)
    cursor.exec
    cursor.fetch() { |row| proposta << row[0].to_i }
    raise "Não tem proposta nesta fase" unless proposta.empty? == false
    return proposta.sample
  end
end

def desgraca
  begin
    page.driver.browser.switch_to.alert
    puts "#{page.driver.browser.switch_to.alert.text} SIM."
    true
  rescue
    p "..."
    return false
  end
end
