class Ficha_cdc_metodos
  include Capybara::DSL

  def preenche_dados_operador
    find("select[name='P_PROMOTOR']").all("option").sample.select_option
    sleep 3
    find("select[name='P_AGENTE_LOJA']").all("option")[1].select_option
    find("select[name='P_AGENTE_LOJA']").all("option")[1].select_option unless find("select[name='P_AGENTE_LOJA']").value.eql?("") == false
  end

  def preenche_dados_pessoais
    find(CAMPOS_PROPOSTA["campo_nome"]).set(FFaker::NameBR.name)
    find(CAMPOS_PROPOSTA["campo_rg_cliente"]).set(FFaker::IdentificationBR.rg)
    find(CAMPOS_PROPOSTA["campo_data_rg_cliente"]).set("01012005")
    find(CAMPOS_PROPOSTA["campo_emissor_rg"]).set("SSP")
    find(CAMPOS_PROPOSTA["campos_estado_cliente"]).find("option[value='SP']").select_option
    find(CAMPOS_PROPOSTA["campo_sexo"]).click
    find(CAMPOS_PROPOSTA["campo_data_nasc"], wait: 7).set("01011992") #<---- DATA NASC
    find(CAMPOS_PROPOSTA["campo_estado_civil"]).find("option[value='2']").select_option
    find(CAMPOS_PROPOSTA["campo_cidade_cliente"]).set("Sao paulo")
    find(CAMPOS_PROPOSTA["campo_natural_de"]).find("option[value='SP']").select_option
    find(CAMPOS_PROPOSTA["campo_nro_depend"]).set("0")
    find(CAMPOS_PROPOSTA["campo_tipo_moradia"]).find("option[value='4']").select_option
    find(CAMPOS_PROPOSTA["campo_valor_imovel"]).set("10000000") #< --------- VALOR DE VENDA ------------
    find(CAMPOS_PROPOSTA["campo_tempo_resid"]).set("01/2015") #<--------- Tempo de residencia ---------
    find(CAMPOS_PROPOSTA["campo_ddd_cliente"]).set("11")
    find(CAMPOS_PROPOSTA["campo_fone_cliente"]).set("58523222")
    find(CAMPOS_PROPOSTA["campo_cat_fone_cliente"]).find("option[value='4']").select_option
    find(CAMPOS_PROPOSTA["campo_cont_fone_cliente"]).set("Teste")
    find(CAMPOS_PROPOSTA["campo_ddd_cel_cliente"]).set("11")
    find(CAMPOS_PROPOSTA["campo_cel_cliente"]).set("981429956")
    find(CAMPOS_PROPOSTA["campo_email_cli"]).set("Teste@teste.com")
    find(CAMPOS_PROPOSTA["campo_mae_cli"]).set("Mae do cliente")
    find(CAMPOS_PROPOSTA["campo_placa_vei"]).set("UYT4521")
    find(CAMPOS_PROPOSTA["campo_renavam"]).set("35135435434")
  end

  
  def preenche_dados_prof
    # IMPLEMENTADO EM SETEMBRO 2020 - FECHA TELA DE ERRO EXIBIDA DUAS VEZES
    # Este código fecha uma tela de Alerta de erro que está sendo exibida após
    # a consulta do CEP. Ela é exibida duas vezes, por isso que tem dois códigos 
    find("#popup_container")
    within("#popup_container") do
      click_button("OK")
    end
    find("#popup_container")
    within("#popup_container") do
      click_button("OK")
    end
    #----------------------------------------------------------------------------
    find("select[name='P_PROF_GRAU_INSTR']").find("option[value='2']").select_option
    find("select[name='P_PROF_CLASSE']").find("option[value='1']").select_option
    find("#P_BSC_PROF").set("ANALISTA DE SISTEMAS")
    find(ELEMENTOS["botao_busca_profissao"]).click
    sleep 5
    find("#P_PROF_EMP").set("Omni")
    find("input[name='P_PROF_SAL']").set("180000")
    find("#P_PROF_TEMP").set("052010")
    find("#P_PROF_FONE_DDD").set("11")
    find("#P_PROF_FONE").set("55889966")
  end

  def confirma_dados_tel
    sleep 2
    find(:xpath, "//input[@name=\"P_PROCEDE_RES\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_RES\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_RES\"]").set("anon")
    # binding.pry
    find(:xpath, "//input[@name=\"P_CONTATO_RES\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_RES\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_RES\"]").set("anon")

    find(:xpath, "//input[@name=\"P_PROCEDE_COM\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_CONTATO_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_COM\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_COM\"]").set("anon")

    find(:xpath, "//input[@name=\"P_PROCEDE_FAM1\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_CONTATO_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_FAM1\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_FAM1\"]").set("anon")
  end

  def confirma_dados_tel_2
    sleep 2
    find(:xpath, "//input[@name=\"P_PROCEDE_RES\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_RES\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_RES\"]").set("anon")

    find(:xpath, "//input[@name=\"P_CONTATO_RES\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_RES\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_RES\"]").set("anon")

    find(:xpath, "//input[@name=\"P_PROCEDE_COM\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_CONTATO_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_COM\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_COM\"]").set("anon")

    find(:xpath, "//input[@name=\"P_PROCEDE_FAM1\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_CONTATO_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_FAM1\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_FAM1\"]").set("anon")
  end

  def preenche_dados_veiculo(int, string)
    find("select[name='P_CATEGORIA#{int}']").find("option", text: MASSA[string]["categoria"]).select_option
    find("select[name='P_MARCA#{int}']").find("option", text: MASSA[string]["marca"]).select_option
    find("select[name='P_ANO_MODELO#{int}']").find("option", text: MASSA[string]["ano"]).select_option
    find("select[name='P_MODELO#{int}']").find("option", text: MASSA[string]["Modelo"]).select_option
    find("select[name='P_VERSAO#{int}']").find("option", text: MASSA[string]["versao"], wait: 10).select_option
  end

  def preenche_proposta_dados_referencias
    find("#P_REF_NOM_FAMILIA").set("contato")
    find("#P_REF_DDD_FAMILIA").set("11")
    find("#P_REF_FONE_FAMILIA").set("58162255")
  end

  def reprocessar_travas
    find(ELEMENTOS["botao_reprocessar_travas"]).click
    assert_selector("#popup_container")
    find("#popup_ok").click
  end

  def preenche_tabelas
    find(ELEMENTOS["Tabela"]).find("option[value='69']").select_option
    find(ELEMENTOS["Produto"]).find("option[value='VEÍCULO LEVE']").select_option
    find(ELEMENTOS["Tipo_credito"]).find("option[value='NORMAL']").select_option
    find(ELEMENTOS["Objeto_financiamento"]).find("option[value='CDC-VL-A-NR-R4']").select_option
  end
end
