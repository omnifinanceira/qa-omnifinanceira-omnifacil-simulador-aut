require "rspec"
require "yaml"
#require "pry"
require "capybara/cucumber"
require "yaml"
#require "ffaker"
#require "capybara-screenshot/cucumber"
require "oci8"

# options =
MASSA = YAML.load_file("data/massa.yml")
ELEMENTOS = YAML.load_file("data/elementos.yml")
CAMPOS_PROPOSTA = YAML.load_file("data/campos_proposta.yml")
MENSAGENS = YAML.load_file("data/mensagens.yml")
BANCO = YAML.load_file("data/db_params.yml")

#Capybara::Screenshot.autosave_on_failure = false
Capybara.default_max_wait_time = 180
# Os ifs abaixo são utilizados quando é digitado o comando para usar um navegador desejado
# exemplo: cucumber chrome=true. Sempre que precisar utilizar um driver de um navegador
# diferente, utilizar o código como no exmeplo acima
if ENV["chrome"]
  Capybara.default_driver = :chrome
  Capybara.register_driver :chrome do |app|
    client = Selenium::WebDriver::Remote::Http::Default.new
    client.read_timeout = 60
    client.open_timeout = 60
    Capybara::Selenium::Driver.new(app, browser: :chrome, http_client: client)
  end
elsif ENV["firefox"]
  Capybara.default_driver = :firefox
  Capybara.register_driver :firefox do |app|
    Capybara::Selenium::Driver.new(app, browser: :firefox)
  end
elsif ENV["ie"]
  Capybara.default_driver = :ie
  Capybara.register_driver :ie do |app|
    Capybara::Selenium::Driver.new(app, browser: :internet_explorer)
  end
elsif ENV["headless_debug"]
  Capybara.default_driver = :poltergeist_debug
  Capybara.register_driver :poltergeist_debug do |app|
    Capybara::Poltergeist::Driver.new(app, inspector: true)
  end
  Capybara.javascript_driver = :poltergeist_debug
elsif ENV["headless"]
  Capybara.javascript_driver = :poltergeist
  Capybara.default_driver = :poltergeist
else
  # Com esta configuração, não é mais necessário digitar cucumbr chrome=true
  Capybara.default_driver = :selenium_chrome
end